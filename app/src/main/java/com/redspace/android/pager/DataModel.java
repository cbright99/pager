package com.redspace.android.pager;

import android.util.Log;

import java.io.Serializable;
import java.util.Observable;

/**
 * Created by cbright on 2017-01-18.
 */

public class DataModel extends Observable implements Serializable {
    private static final String TAG = "DataModel";

    private static DataModel ourInstance = new DataModel();

    public static DataModel getInstance() {
        return ourInstance;
    }

    private DataModel() { }

    // Variables
    private String message = "";
    private String alertWords = "";
    private int volume = 0;
    private int alertSoundIndex = 0;

    /////////////////////////////////////////////////////////

    public void broadcast() {
        setChanged();
        notifyObservers();
    }

    /////////////////////////////////////////////////////////

    public void setMessage(String message) {
        synchronized (this) { this.message = message; }
        broadcast();
    }

    public synchronized String getMessage() {
        return message;
    }

    public void setAlertWords(String alertWords) {
        synchronized (this) { this.alertWords = alertWords; }
        //broadcast();
    }

    public synchronized String getAlertWords() {
        return alertWords;
    }

    public void setVolume(int volume) {
        synchronized (this) { this.volume = volume; }
        //broadcast();
    }

    public synchronized int getVolume() {
        return volume;
    }

    public void setAlertSoundIndex(int alertSoundIndex) {
        synchronized (this) { this.alertSoundIndex = alertSoundIndex; }
        //broadcast();
    }

    public synchronized int getAlertSoundIndex() {
        return alertSoundIndex;
    }

    /////////////////////////////////////////////////////////

    public void loadStoredDataModel(DataModel dm) {
        Log.d(TAG, "loadStoredDataModel: dm.message = " + dm.message );

        ourInstance.message = dm.message;
        ourInstance.alertWords = dm.alertWords;
        ourInstance.volume = dm.volume;
        ourInstance.alertSoundIndex = dm.alertSoundIndex;

        broadcast();
    }

}

