package com.redspace.android.pager;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by cbright on 2017-01-25.
 */

public final class DataModelUtil {
    private static final String TAG = "DataModelUtil";

    private DataModelUtil() {
        // empty constructor
    }

    public static void saveDataModel(Context ac, DataModel tm) {
        try {
            FileOutputStream fos = ac.openFileOutput("pagerfile", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(tm);
            os.close();
            fos.close();
            Log.d(TAG, "save: finished");
        }
        catch (IOException ioe) {
            Log.e(TAG, "save: IOException", ioe);
        }
    }

    public static void loadDataModel(Context ac) {
        DataModel dataModel = null;
        DataModel storedDataModel = null;

        try {
            FileInputStream fis = ac.openFileInput("pagerfile");
            ObjectInputStream is = new ObjectInputStream(fis);
            storedDataModel = (DataModel) is.readObject();
            is.close();
            fis.close();
        } catch (ClassNotFoundException cnfe) {
            Log.e(TAG, "onCreate: ClassNotFoundException");
        } catch (IOException ioe) {
            Log.e(TAG, "onCreate: IOException", ioe);
        }

        dataModel = DataModel.getInstance();

        if (storedDataModel != null) {
            dataModel.loadStoredDataModel(storedDataModel);
        }
    }
}
