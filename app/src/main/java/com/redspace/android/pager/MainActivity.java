package com.redspace.android.pager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    private static String TAG = "MainActivity";

    private SmsListener smsListener;
    private IntentFilter intentFilter;

    private DataModel dataModel;

    private TextView messageTV;
    private Button sendButton;
    private Button updateButton;
    private EditText alertWordsET;
    private SeekBar volumeBar;
    private RadioGroup radioAlertSoundGroup;
    RadioButton rbu1;
    RadioButton rbu2;
    RadioButton rbu3;
    RadioButton rbu4;
    RadioButton rbu5;

    private int alertSoundId = R.raw.siren;
    private String alertSoundString = "Loud Siren";

    MediaPlayer mediaPlayer;

    private AudioManager mobilemode;
    private float alertVolume = 1/15f;
    private int maxMusicVolume = 15;
    private int maxRingVolume = 15;

    private static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1234;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.context = this.getApplicationContext();

        // need to ask the user if they will let this app read messages
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "No permission");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECEIVE_SMS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }

        setContentView(R.layout.activity_main);

        messageTV = (TextView) findViewById(R.id.messageTV);
        sendButton = (Button) findViewById(R.id.sendButton);
        updateButton = (Button) findViewById(R.id.updateButton);
        alertWordsET = (EditText) findViewById(R.id.alertWordsET);

/*
        alertWordsET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                //dataModel.setAlertWords(alertWordsET.getText().toString());
            }
            public void beforeTextChanged(CharSequence s, int start,  int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
*/

        volumeBar = (SeekBar) findViewById(R.id.volumeBar);
        radioAlertSoundGroup = (RadioGroup) findViewById(R.id.radioAlertSound);

        /*
        radioAlertSoundGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                callUpdateSettingsCall();
            }
        });
        */

        rbu1 = (RadioButton)findViewById(R.id.soundSiren); rbu1.setSelected(true);
        rbu2 = (RadioButton)findViewById(R.id.soundDoorbell); rbu2.setSelected(true);
        rbu3 = (RadioButton)findViewById(R.id.soundFoghorn); rbu1.setSelected(false); rbu3.setSelected(true);
        rbu4 = (RadioButton)findViewById(R.id.soundShipbell); rbu4.setSelected(true);
        rbu5 = (RadioButton)findViewById(R.id.soundSong);

        mobilemode = (AudioManager)this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        maxMusicVolume = mobilemode.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        maxRingVolume = mobilemode.getStreamMaxVolume(AudioManager.STREAM_RING);

        setVolumeBarChangeListener();

        loadDataModel();
    }

    public void observe(Observable o) {
        o.addObserver(this);
    }

    public void unObserve(Observable o) {
        o.deleteObserver(this);
    }

    @Override
    public void update(final Observable o, Object arg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String message = ((DataModel) o).getMessage();
                Log.d(TAG, "update: run: message = " + message);

                String alertWords = ((DataModel) o).getAlertWords();
                alertWordsET.setText(alertWords);

                int volume = ((DataModel) o).getVolume();
                volumeBar.setProgress(volume);

                int radioId = ((DataModel) o).getAlertSoundIndex();

                switch (radioId) {
                    case 0 : rbu1.setChecked(true); break;
                    case 1 : rbu2.setChecked(true); break;
                    case 2 : rbu3.setChecked(true); break;
                    case 3 : rbu4.setChecked(true); break;
                    case 4 : rbu5.setChecked(true); break;
                    default: rbu1.setChecked(true);
                }

                if (message.length()>0) {
                    messageTV.setText(message);
                    saveDataModel();

                    if (message.toLowerCase().replaceAll(" ","").contains(alertWords.toLowerCase().replaceAll(" ",""))) {
                        increaseVolume();
                        playSound();
                        //bringToFront();
                    }
                }
            }
        });
    }

    private void increaseVolume() {
        int musicVolume = (int) (maxMusicVolume * alertVolume);
        int ringVolume = (int) (maxRingVolume * alertVolume);

        Log.d(TAG, "increaseVolume: volume = " + musicVolume +"/" + maxMusicVolume);

        mobilemode.setStreamVolume(AudioManager.STREAM_MUSIC,musicVolume,0);
        mobilemode.setStreamVolume(AudioManager.STREAM_RING,ringVolume,0);
    }

    // works
    private void playSound() {
        Log.d(TAG, "playSound:");
        stopPlaying();
        mediaPlayer = MediaPlayer.create(this.getApplicationContext(),alertSoundId);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public void stopAlert(View view) {
        stopPlaying();
    }

    public void stopPlaying() {
        Log.d(TAG, "stopAlert:");
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
    public void updateSettings(View view) {
        callUpdateSettingsCall();

        String alertWords = alertWordsET.getText().toString();
        int percentVolume = (int)(alertVolume*100);

        Toast.makeText(getApplicationContext(), "Listening for \"" + alertWords + "\", playing \"" +
                alertSoundString + "\" at volume = " + percentVolume + "%", Toast.LENGTH_SHORT).show();
    }

    public void callUpdateSettingsCall() {
        Log.d(TAG, "updateSettings: " + alertWordsET.getText().toString());

        // get selected radio button from radioGroup
        int selectedId = radioAlertSoundGroup.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        RadioButton radioAlertSoundButton = (RadioButton) findViewById(selectedId);

        int radioId = radioAlertSoundGroup.indexOfChild(radioAlertSoundButton);
        int oldRadioId = dataModel.getAlertSoundIndex();
        if (dataModel.getAlertSoundIndex() != radioId) { dataModel.setAlertSoundIndex(radioId); }

        switch (radioId) {
            case 0 : alertSoundId = R.raw.siren; break;
            case 1 : alertSoundId = R.raw.doorbell; break;
            case 2 : alertSoundId = R.raw.foghorn; break;
            case 3 : alertSoundId = R.raw.shipbell; break;
            case 4 : alertSoundId = R.raw.song; break;
            default: alertSoundId = R.raw.siren;
        }

        alertSoundString = radioAlertSoundButton.getText().toString();

        String alertWords = alertWordsET.getText().toString();
        dataModel.setAlertWords(alertWords);

        saveDataModel();
    }

    public void sendFakeMessage(View view) {
        dataModel.setMessage("Fake Play Siren Message - Fake Play Siren Message - Fake Play Siren Message - Fake Play Siren Message - Fake Play Siren Message");
    }

    private void setVolumeBarChangeListener() {
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                dataModel.setVolume(progressValue);
                //saveDataModel();
                alertVolume = progressValue/15f;
                Log.d(TAG, "onProgressChanged: Changing seekbar's progress - alertVolume = " + alertVolume);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Toast.makeText(getApplicationContext(), "Stopped tracking seekbar - alertVolume = " + alertVolume, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadDataModel() {
        DataModelUtil.loadDataModel(this.getApplicationContext());

        dataModel = DataModel.getInstance();
        observe(dataModel);
        dataModel.broadcast(); // broadcast to make sure we observe the current state to start
    }

    public void saveDataModel() {
        DataModelUtil.saveDataModel(this.getApplicationContext(),dataModel);
        //dataModel.setSaveDataModel(false);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    // works
    /*
    private void playRingtone() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }
    */

    // doesn't work
    private void bringToFront() {
        /*
        Intent intent = new Intent(this.getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        */

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // You need this if starting
        // the activity from a service
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        startActivity(intent);
    }

}
