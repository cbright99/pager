package com.redspace.android.pager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by cbright on 2017-01-18.
 */

public class SmsListener extends BroadcastReceiver {

    private static String TAG = "SmsListener";

    public SmsListener() {
        Log.d(TAG, "Constructor");
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                String messageBody = smsMessage.getMessageBody();
                Log.d(TAG, "onReceive: message = " + messageBody);

                DataModel dataModel = DataModel.getInstance();
                dataModel.setMessage(messageBody);
            }
        }
    }
}